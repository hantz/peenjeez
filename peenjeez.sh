#!/usr/bin/env bash

# Exit if less than 2 arguments
: ${2?"Usage: $0 input output_prefix [config_file]"}

while read -r line
do
    # Split on "x" for target dimensions
    dimensions=($(awk -Fx '{$1=$1} 1' <<<"${line}"))

    # Malformed config_file, or argument (e.g. 12_12, 12, 12, _12
    if [ "${#dimensions[@]}" -lt 2 ]; then
        echo "Missing height or width. Skipping."
    else
        w="${dimensions[0]}"
        h="${dimensions[1]}"

        # Check if arguemnts are numbers
        if [[ "${w}" == ?(-)+([0-9]) ]] && [[ "${h}" == ?(-)+([0-9]) ]]; then
            # Good: 12x_12
            inkscape -z -e $2_"${w}"x"${h}".png  -w "${w}" -h "${h}" $1
        elif [[ "${dimensions[0]}" == ?(-)+([0-9]) ]]; then
            # Good: 12x_
            inkscape -z -e $2_"${w}".png -w "${w}" $1
        elif [[ "${dimensions[1]}" == ?(-)+([0-9]) ]]; then
            # Good: _x12
            inkscape -z -e $2_"${h}".png -h "${h}" $1
        else
            # Enough arguments but not valid ones 9not numbers). (e.g. wordxword
            # wordx_, _xword
            echo "Invalid arguements. Skipping."
        fi
    fi
done < "${3:-/dev/stdin}"

# peenjeez

pngize :\^)

Simple script to convert svg files to png in multiple format.

## Usage

```
$ peenjeez input output_prefix [optional_config]
```

or

```
$ peenjeez input output_prefix
## give dimension one at a time from the commadline

widthxheight
_xheight
heightx_
```

The width and height parts of the dimension must be separated by the `x`
charachter.
To omit the height or the width, pass a `_` (e.g. `512x_` or `_x1024`)
The optional config file consist of a list of dimension, each on their own line.
See example below.

### Arguments

- input: SVG file to convert
- output_prefix: Prefix to use for the produced png files (files are named
  prefix__widthxheight.png)
- config: Optional config file to read dimensions from

### Example

```
$ peenjeez input.svg output_prefix 512x512
```

#### Valid config file example

```
256x256
512x_
_x1024
```

## Dependencies

- inkscape
